1. Marjorie Green Books:
	-The Busy Executive's Database Guide
	-You Can Combat Computer Stress!

2. Michael O'Leary Books:
	-Cooking with Computers
	-TC7777: Error! Book not found!

3. The Busy Executive's Database Guide Authors:
	-Marjorie Green
	-Abraham Bennet

4. Publisher of But Is It User Friendly?:
	-Algodata Infosystems

5. Books published by Algodata Infosystems:
	-The Busy Executive's Database Guide
	-Cooking with Computers
	-Straight Talk About Computers
	-But Is It User Friendly?
	-Secrets of Silicon Valley
	-Net Etiquette